import { trigger, transition, group, stagger } from '@angular/animations';
import { animate, style } from '@angular/animations';
import { useAnimation, query, animateChild } from '@angular/animations';
import { Component } from '@angular/core';
import { bounceOutLeftAnimation, fadeInAnimation } from 'app/animations';

@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
  animations: [
    trigger('todosAnimation', [
      transition(':enter', [
        // use group() to run multiple animations simultaneously
        group([
          // animate(1000, style({ background: 'red' })),
          // animate(2000, style({ transform: 'translateY(50px)' }))
          query('h1', [
            style({ transform: 'translateY(-20px)' }),
            animate('1s')
          ]),
          // execute the @todoAnimation, which is a child of @todosAnimation
          query('@todoAnimation', stagger('200ms', animateChild()))
          // query(
          //   '.list-group-item',
          //   stagger('200ms', [
          //     style({ opacity: 0, transform: 'translateX(-20px)' }),
          //     animate('1000ms')
          //   ])
          // )
        ])
      ])
    ]),
    trigger('todoAnimation', [
      transition(':enter', [
        useAnimation(fadeInAnimation, {
          // override the default input values of fadeInAnimation() with the params set here.
          params: {
            duration: '500ms'
          }
        })
      ]),
      transition(':leave', [
        style({ backgroundColor: 'red' }),
        animate(500),
        useAnimation(bounceOutLeftAnimation)
      ])
    ])
  ]
})
export class TodosComponent {
  items: any[] = [
    'Wash the dishes',
    'Call the accountant',
    'Apply for a car insurance'
  ];

  addItem(input: HTMLInputElement) {
    this.items.splice(0, 0, input.value);
    input.value = '';
  }

  removeItem(item) {
    const index = this.items.indexOf(item);
    this.items.splice(index, 1);
  }

  animationStarted($event) {
    console.log(event);
  }

  animationDone($event) {
    console.log(event);
  }
}
