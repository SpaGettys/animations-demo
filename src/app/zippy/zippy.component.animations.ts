import { trigger, transition, group, stagger } from '@angular/animations';
import { animate, style, state } from '@angular/animations';
import { useAnimation, query, animateChild } from '@angular/animations';
import { animation } from '@angular/core/src/animation/dsl';

export const expandCollapse = trigger('expandCollapse', [
  state(
    'collapsed',
    style({
      height: 0,
      paddingTop: 0,
      paddingBottom: 0,
      opacity: 0
      /* backgroundColor: 'yellow' */
    })
  ),
  // Not necessary, because Angular will handle it for us automatically as it is the 'default'
  // state of the DOM before we apply the collapsed state.
  // state(
  //   'expanded',
  //   style({
  //     height:
  //       '*' /* angular will calculate automatically at run-time for us based on the height the DOM */,
  //     padding:
  //       '*' /* the padding defined in zippy.component.css will come into effect */,
  //     overflow: 'auto'
  //   })
  // ),
  transition('collapsed => expanded', [
    // group([
    animate(
      '100ms ease-out',
      style({
        height: '*',
        paddingTop: '*',
        paddingBottom: '*'
      })
    ),
    animate('1s', style({ opacity: 1 }))
    // ])
  ]),
  transition('expanded => collapsed', [animate('300ms ease-in')])
]);
