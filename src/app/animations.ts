import {
  trigger,
  state,
  transition,
  animate,
  style,
  keyframes,
  animation,
  useAnimation
} from '@angular/animations';

// use the animation() function to create re-usaable animations. and uise the useAnimation() method to call them :D
// reusable animation function
export let bounceOutLeftAnimation = animation(
  // animate('0.5s ease-in', style({ transform: 'translateX(-100%)' }))
  animate(
    '0.5s cubic-bezier(.55, .04, .92, .68)',
    // style({ transform: 'translateX(-100%)' })
    keyframes([
      style({
        offset: 0.2,
        opacity: 1,
        transform: 'translateX(20px)'
      }),
      style({
        offset: 1,
        opacity: 0,
        transform: 'translateX(-100%)'
      })
    ])
  )
);

export let slide = trigger('slide', [
  transition(':enter', [
    style({ transform: 'translateX(-10px)' }),
    animate(500)
  ]),

  transition(':leave', useAnimation(bounceOutLeftAnimation))
]);

// reusable animation function
export let fadeInAnimation = animation(
  [style({ opacity: 0 }), animate('{{ duration }} {{ easing }}')],
  {
    // set the default values for the paramaters
    params: {
      duration: '2s',
      easing: 'ease-out'
    }
  }
);

// reusable animation function
export let fadeOutAnimation = animation(
  [animate('{{ duration }} {{ easing }}', style({ opacity: 0 }))],
  {
    // set the default values for the paramaters
    params: {
      duration: '2s',
      easing: 'ease-in'
    }
  }
);

// reusable trigger
export let fade = trigger('fade', [
  // transition('void <=> *', [animate(2000)]) // this works too
  // transition('void => *, * => void', [animate(2000)]) // this works too
  transition(':enter', useAnimation(fadeInAnimation)), // alias version, and more readable in Mosh's opinion.
  transition(':leave', useAnimation(fadeOutAnimation)) // alias version, and more readable in Mosh's opinion.
]);
